<?php
    session_start();
    
    if(isset($_SESSION['planta'])){
        header("Location: index.php");
        exit;
    }
        
        
?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <center><h1>Login</h1></center>
            </div>
            <br />
            <br />
            <form>
                <div class="row">
                    <div class='col-xs-offset-2 col-xs-8'>
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'>Usuario:</div>
                            <div class='col-xs-4'>
                               <input type="text" class="form-control" id="user" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'>Contraseña:</div>
                            <div class='col-xs-4'>
                                <input type="password" class="form-control" id="password" />

                            </div>
                            <div class='col-xs-3'>
                            </div>
                        </div>
                        <br />
                        
                    </div>
                </div>
                <div class="row">
                <center><a href="#" class="lnk"><button class='btn btn-success' id='btn-login' >Entrar</button></a></center>
            </div>
            </form>
            <br /><br /><br />
            
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 
    jQuery(document).ready(function(){
        jQuery("#btn-login").on("click", function(){
            let user = jQuery("#user");
            let pass = jQuery("#password");
            if(user.val() == ""){
                user[0].setCustomValidity("Campo Requerido");
            }else if(pass.val() == user.val()){
                $.ajax({
                    method: "POST",
                    url: "rev.php?user="+user.val(),
                    dataType:"json"
                })
                .done(function( msg ) {
                    if(msg){
                        location.href="cambio.php"
                    }else{
                        alert("No coinciden los datos");
                    }
                });
            }else{
                $.ajax({
                    method: "POST",
                    url: "check.php?user="+user.val()+"&password="+pass.val(),
                    dataType:"json"
                })
                .done(function( msg ) {
                    if(msg){
                        location.href="index.php"
                    }else{
                        alert("No coinciden los datos");
                    }
                });
            }
        });
    }); 
    </script>
</htmL>