<?php
    session_start();
    
    if(!isset($_SESSION['cambio'])){
        header("Location: index.php");
        exit;
    }
        
        
?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <center><h1>Cambiar Contraseña <?php echo $_SESSION['cambio']  ?></h1></center>
            </div>
            <br />
            <br />
            <form>
                <div class="row">
                    <div class='col-xs-offset-2 col-xs-8'>
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'>Contraseña:</div>
                            <div class='col-xs-4'>
                               <input type="password" class="form-control" id="password" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'>Repetir Contraseña:</div>
                            <div class='col-xs-4'>
                                <input type="password" class="form-control" id="password1" />

                            </div>
                            <div class='col-xs-3'>
                            </div>
                        </div>
                        <br />
                        
                    </div>
                </div>
                <div class="row">
                <center><a href="#" class="lnk"><button class='btn btn-warning' id='btn-cambiar'>CAMBIAR</button></a></center>
            </div>
            </form>
            <br /><br /><br />
            
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 
    jQuery(document).ready(function(){

        jQuery("#btn-cambiar").on("click", function(){
            let pass = jQuery("#password").val();
            let pass1 = jQuery("#password1").val();
            if(pass == ""){
                this.setCustomValidity("Campo Requerido");
            }else if(pass.length < 8){
                this.setCustomValidity("Se requieren minimo 8 caracteres");

            }else if(pass != pass1){
                this.setCustomValidity("No coinciden las contraseñas");
            }else{
                
                $.ajax({
                    method: "POST",
                    url: "update.php?user=<?php echo $_SESSION['cambio'] ?>&password="+pass,
                    dataType:"json"
                })
                .done(function( msg ) {
                    if(msg){
                        location.href="index.php"
                    }else{
                        alert("Error al actualizar la contraseña");
                    }
                });


            }
        });
    }); 
    </script>
</htmL>