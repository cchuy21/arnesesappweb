<?php
    session_start();
    include('conexion.php');
    $sql = "SELECT planta FROM users_app group by planta";
    if(!isset($_SESSION['planta'])){
        header("Location: login.php");
        exit;
    }
        
        
?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body> 
        <a href="logout.php"><button class="btn btn-large btn-danger" style="position:absolute;right:0px;">&nbsp;&nbsp;&nbsp;SALIR&nbsp;&nbsp;&nbsp;</button></a>
        <div class="container">
            <div class="row">
                <center><h1>Resultados App-Covid <?php echo $_SESSION['planta'] ?></h1></center>
            </div>
            <br />
            <br />
            <div class="row">
                <form method='POST' action="excel.php">
                    <div class='col-xs-offset-2 col-xs-8'>
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'></div>
                            <div class='col-xs-4'>
                                <input name='planta' class="form-control" type="hidden" id="planta" value="<?php echo $_SESSION['planta'] ?>">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'>Resultado:</div>
                            <div class='col-xs-4'>
                                <select name='tipo' class="form-control" id="tipo">
                                    <option value='verde'>POSITIVO(verde)</option>
                                    <option value='naranja'>ALERTA(naranja)</option>
                                    <option value='rojo'>NEGATIVOS(rojo)</option>
                                </select>
                            </div>
                            <div class='col-xs-3'>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class='col-xs-offset-1 col-xs-3'>Fecha:</div>
                            <div class='col-xs-4'>
                                <input type="date" class="form-control" name="fecha" min="2021-02-22" value='<?php echo date("Y-m-d"); ?>' id="dates" />
                            </div>
                            <div class='col-xs-3'>
                                <input type="submit" class='btn btn-success' style='color:white!important;' id="boton" value='Descargar Excel' />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <br /><br /><br />
            <div class="row">
                <center><a href="#" class="lnk"><button class='btn btn-danger' id='btn-ver' >Ver Resultados</button></a></center>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 
    jQuery(document).ready(function(){

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 

        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("dates").setAttribute("max", today);
        
        jQuery("select").on("click", function(){
            let pl = jQuery("#planta").val();
            let tipo = jQuery("#tipo").val();
            let fec = jQuery("#dates").val();
            jQuery(".lnk").attr("href","ver.php?planta="+pl+"&tipo="+tipo+"&fecha="+fec);
        });
        jQuery("input").on("click", function(){
            let pl = jQuery("#planta").val();
            let tipo = jQuery("#tipo").val();
            let fec = jQuery("#dates").val();
            jQuery(".lnk").attr("href","ver.php?planta="+pl+"&tipo="+tipo+"&fecha="+fec);
        });
        let pl = jQuery("#planta").val();
            let tipo = jQuery("#tipo").val();
            let fec = jQuery("#dates").val();
            jQuery(".lnk").attr("href","ver.php?planta="+pl+"&tipo="+tipo+"&fecha="+fec);
    }); 
    </script>
</htmL>