<?php
        include('conexion.php');
?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <center><h1>Resultados <?php echo $_GET['tipo'] ?> App-Covid <?php echo $_GET['planta'] ?> <?php echo date("d/m/Y",strtotime($_GET['fecha'])) ?></h1></center>
                <br />
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nómina</th>
                            <th>Nombre</th>
                            <th>Planta</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
                    $sql = "SELECT * FROM users_app where id IN (select user from resultados_app where valor = '".$_GET['tipo']."' and fecha = '".$_GET['fecha']."') and planta ='".$_GET['planta']."' order by id asc";
                    $c = 1;
                    
                    
                    if ($result = $mysqli->query($sql)) {
                        while($obj = $result->fetch_object()){
                            $porciones = explode(",", $obj->nombre);
                            echo "<tr>
                                <td>".$obj->id."</td>
                                <td>".$porciones[0]." ".$porciones[1]."</td>
                                <td>".$obj->planta."</td>
                            </tr>";
                        }
                    }
                ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 
    jQuery(document).ready(function(){
    });
    </script>
</htmL>