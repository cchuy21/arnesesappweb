<?php
session_start();
include('conexion.php');
if(!isset($_GET['user']) && !isset($_SESSION['user'])){
    header("Location: login.php");
    exit;
}
if(isset($_GET['user']) && !isset($_SESSION['user'])){
    $_SESSION['user'] = $_GET['user'];
    header("Location: index.php");
    exit;
}

?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body class="inicio">
        <div class="container">
            <br />
            <div class="row">
                <h1>Aviso Importante</h1>
            </div>
            <br />
            <br />
            <div class="row">
                Las respuestas proporcionadas en este cuestionario serán utilizadas únicamente para efectos de la empresa y control sobre el estado médico de los empleados.
            </div>
            <br />
            <br />
            <div class="row">
                Al dar click en "ENTENDIDO" te comprometes a contestar cada una de las preguntas con honestidad permitiendo la mejora de las prácticas en Sistemas de Arneses K&S cuidando tu salud y la de todo nuestro equipo de trabajo.
            </div>
            <br />
            <br />
            <div class="row">
                Sus datos son confidenciales y estarán protegidos por nuestro AVISO DE PRIVACIDAD Lo puede consultar en:     
            </div>
            <div class="row">
                <a href="https://www.ksmex.com.mx">www.ksmex.com.mx</a>
            </div>
            <br />
            <br />
            <div class="row">
                <a href="menu.php"><button id="btn-ok">ENTENDIDO</button></a>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 

    </script>
</htmL>