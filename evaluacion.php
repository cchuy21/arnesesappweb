<?php
session_start();
include('conexion.php');
if(!isset($_SESSION['user'])){
    header("Location: login.php");
    exit;
}

?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body class="eva">
        <img src="assets/KSAPP_LOGOTIPO.png" id="logoizq" />
        <div class="row ev">
            <h1><?php echo $_SESSION['user'] ?></h1>
        </div>
        <br />
        <br />
        
        <div class="container">
            <br />
            <h1>MENÚ</h1>
            <br />
            <br />

            <div class="row">
                <a href="evaluacion.php"><button class="btn btn-primary btn-menu">EVALUACIÓN</button></a>
            </div>
            <div class="row">
                <button class="btn btn-primary btn-menu">HISTORIAL DE <br />REGISTRO</button>
            </div>
            <div class="row">
                <a href="logout.php"><button class="btn btn-primary btn-menu">CERRAR SESIÓN</button></a>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 


preguntas = [
    {
      pregunta: '¿Ha tenido algún tipo de síntomas (Fiebre, Tos, Dificultad para respirar, Congestión nasal, Dolor de cabeza, garganta o músculos, Molestias estomacales, Sarpullido, Perdida del olfato o gusto o algún otro síntoma relacionado con COVID-19) en los últimos 3 días?',
      img: '../../../assets/images/KSAPP_ILUSTACIONP1.png',
      respuestas: [
        {
          label: 'SI',
          valor: 3
        },
        {
          label: 'NO',
          valor: 0
        }
      ]
    },
    {
      pregunta: '¿Ha estado en contacto cercano con algún familiar o amigo que haya presentado los síntomas previamente descritos en los últimos 3 días?',
      img: '../../../assets/images/KSAPP_ILUSTACIONP2.png',
      respuestas: [
        {
          label: 'SI',
          valor: 1
        },
        {
          label: 'NO',
          valor: 0
        }
      ]
    },
    {
      pregunta: '¿Ha estado en contacto con alguien confirmado de COVID-19 en prueba PCR (con el cotonete en garganta y nariz) en los últimos 3 días?',
      img: '../../../assets/images/KSAPP_ILUSTACIONP3.png',
      respuestas: [
        {
          label: 'SI',
          valor: 3
        },
        {
          label: 'NO',
          valor: 0
        }
      ]
    },
    {
      pregunta: '¿Ha estado en contacto con alguien sospechoso de Covid-19 en los últimos 3 días?',
      img: '../../../assets/images/KSAPP_ILUSTACIONP4.png',
      respuestas: [
        {
          label: 'SI',
          valor: 1
        },
        {
          label: 'NO',
          valor: 0
        }
      ]
    },
    {
      pregunta: '¿Ha estado en lugares concurridos donde no se respetaron los protocolos de prevención? (uso de cubrebocas en todo momento. Distancia de 1.5 m, lavado de manos, etc…) en los últimos 3 días',
      img: '../../../assets/images/KSAPP_ILUSTACIONP5.png',
      respuestas: [
        {
          label: 'SI',
          valor: 1
        },
        {
          label: 'NO',
          valor: 0
        }
      ]
    },
    {
      pregunta: 'Tiene alguna de las siguientes condiciones (Hipertensión, Diabetes, Obesidad tipo 3, Embarazo, enfermedades cardiovasculares, toma de inmunosupresores, enfermedades respiratorias)',
      img: '../../../assets/images/KSAPP_ILUSTACIONP6.png',
      respuestas: [
        {
          label: 'SI',
          valor: 1
        },
        {
          label: 'NO',
          valor: 0
        },
        {
          label: 'Sí, pero ya fui validado por el medico',
          valor: 0
        }
      ]
    },
    {
      pregunta: '¿En caso de haber dado positivo a COVID-19, Se le ha otorgado el alta médica?',
      img: '../../../assets/images/KSAPP_ILUSTACIONP7.png',
      respuestas: [
        {
          label: 'SI',
          valor: 0
        },
        {
          label: 'NO',
          valor: 3
        },
        {
          label: 'No he sido diagnosticado con COVID-19',
          valor: 0
        }
      ]
    },
    {
      pregunta: '¿En caso de haber tenido que cumplir aislamiento, ha sido autorizado formalmente por el seguimiento médico para ingresar a la planta?',
      img: '../../../assets/images/KSAPP_ILUSTACIONP8.png',
      respuestas: [
        {
          label: 'SI',
          valor: 0
        },
        {
          label: 'NO',
          valor: 3
        },
        {
          label: 'No he sido enviado a cuarentena',
          valor: 0
        }
      ]
    },
  ];

  preguntas2 = [
    {
      pregunta: '最近３日以内で次の症状がありますか。発熱、咳、 呼吸困難、 鼻詰まり、 頭痛-喉の痛み-筋肉痛-胃の痛み、発疹、 味覚または嗅覚の消失、COVID-19に似た他の症状',
      img: '../../../assets/images/KSAPP_ILUSTACIONP1.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 3
        },
        {
          label: 'いいえ​',
          valor: 0
        }
      ]
    },
    {
      pregunta: '最近３日以内で、上記の症状のある人と接触したことがありますか？',
      img: '../../../assets/images/KSAPP_ILUSTACIONP2.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 1
        },
        {
          label: 'いいえ​',
          valor: 0
        }
      ]
    },
    {
      pregunta: '最近3日以内で、新型コロナ感染者と接触したことがありますか？（PCR検査）​',
      img: '../../../assets/images/KSAPP_ILUSTACIONP3.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 3
        },
        {
          label: 'いいえ​',
          valor: 0
        }
      ]
    },
    {
      pregunta: '最近３日以内で感染の疑いがある人と接触したことがありますか。​',
      img: '../../../assets/images/KSAPP_ILUSTACIONP4.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 1
        },
        {
          label: 'いいえ​',
          valor: 0
        }
      ]
    },
    {
      pregunta: '最近3日以内で、感染防止対策が守られていない混雑した場所にいましたか？（マスクの使用、1.5mの距離、手洗いなど...）',
      img: '../../../assets/images/KSAPP_ILUSTACIONP5.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 1
        },
        {
          label: 'いいえ​',
          valor: 0
        }
      ]
    },
    {
      pregunta: '次の病状がありますか？高血圧、糖尿病、心血管疾患、免疫抑制剤の服用、呼吸器疾患',
      img: '../../../assets/images/KSAPP_ILUSTACIONP6.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 1
        },
        {
          label: 'いいえ​',
          valor: 0
        },
        {
          label: 'ありますが医療機関からの許可を得ています​',
          valor: 0
        }
      ]
    },
    {
      pregunta: 'Covid-19に感染していた場合、もう退院しましたか。​',
      img: '../../../assets/images/KSAPP_ILUSTACIONP7.png',
      respuestas: [
        {
          label: 'はい​',
          valor: 0
        },
        {
          label: 'いいえ​',
          valor: 3
        },
        {
          label: 'COVID-19と診断されていません。',
          valor: 0
        }
      ]
    },
    {
      pregunta: '隔離されていた場合、会社のドクターから会社に戻る為の許可を頂きましたか。',
      img: '../../../assets/images/KSAPP_ILUSTACIONP8.png',
      respuestas: [
        {
          label: 'SI',
          valor: 0
        },
        {
          label: 'NO',
          valor: 3
        },
        {
          label: '私は隔離されていません。​',
          valor: 0
        }
      ]
    },
  ];










    </script>
</htmL>