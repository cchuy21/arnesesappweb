var car;
var dta = {};

jQuery(document).ready(function(){
    car = jQuery("#cuest").val();
    cargar();
    jQuery(".dng").off("click").on("click", function() {
        jQuery(".inp:checked:not(:disabled)").each( function(){
            dta[jQuery(this).data('id')] = jQuery(this).val();
        });
        
        cargar();
    });
});

function inp(c){
    if(car == 1){
        console.log(c);
        jQuery(".inp[name=rad2]").off("change");
        jQuery(".inp[name=rad2]").on("change", function(){
            console.log("entro "+ jQuery(this).val());
            if(jQuery(this).val() == "Si"){
                jQuery(".dng").removeClass("btn-success");
                jQuery(".dng").addClass("btn-danger");
                jQuery(".dng").html("Siguiente");
                jQuery(".dng").off("click");
                jQuery(".dng").off("click").on("click", function() {
                    jQuery(".inp:checked:not(:disabled)").each( function(){
                        dta[jQuery(this).data('id')] = jQuery(this).val();
                    });
                    cargar();
                });
                jQuery(".dng").prop("disabled", true);
                for(var d1 = 3;d1 <= 7; d1++){
                    jQuery(".row" + d1).css("display","block");
                    jQuery(".inp[name=rad" + d1 + "]").prop("disabled", false);
                }
                
            }else{
                jQuery(".dng").removeClass("btn-danger");
                jQuery(".dng").addClass("btn-success");
                jQuery(".dng").html("Guardar");
                jQuery(".dng").prop("disabled", false);

                jQuery(".dng").off("click").on("click", function() {
                    jQuery(this).prop("disabled", true);
                    jQuery(".inp:checked:not(:disabled)").each( function(){
                        dta[jQuery(this).data('id')] = jQuery(this).val();
                    });
                    dta['user'] = jQuery("#us-id").val();
                    jQuery.ajax({
                        method: "POST",
                        dataType: "json",
                        data: dta,
                        url: "save.php"
                    }).done(function(data) {
                        if(data){
                            window.location.href = "finalizar.php";
                        }else{
                            jQuery(this).prop("disabled", false);
                            alert("error intente mas tarde");
                        }
                    }).fail(function() {
                        jQuery(this).prop("disabled", false);
                        alert("error intente mas tarde");
                    });
                });
                for(var d1 = 3;d1 <= 7; d1++){
                    jQuery(".row" + d1).css("display","none");
                    jQuery(".inp[name=rad" + d1 + "]").prop("disabled", true);
                }
            }
        });
    }else
    jQuery(".inp").off("change");
    jQuery(".inp").on("change", function() {
        var c2 = 0;
        jQuery(".inp:checked:not(:disabled)").each( function(){
            c2++;
            if(c == c2) {
                jQuery(".dng").prop("disabled", false);
                if(car == 4){
                    jQuery(".dng").removeClass("btn-danger");
                    jQuery(".dng").addClass("btn-success");
                    jQuery(".dng").html("Guardar");
                    jQuery(".dng").off("click").on("click", function() {
                        jQuery(this).prop("disabled", true);
                        jQuery(".inp:checked:not(:disabled)").each( function(){
                            dta[jQuery(this).data('id')] = jQuery(this).val();
                        });
                        dta['user'] = jQuery("#us-id").val();
                        jQuery.ajax({
                            method: "POST",
                            dataType: "json",
                            data: dta,
                            url: "save.php"
                        }).done(function(data) {
                            if(data){
                                window.location.href = "finalizar.php";
                            }else{
                                jQuery(this).prop("disabled", false);
                                alert("error intente mas tarde");
                            }
                        }).fail(function() {
                            jQuery(this).prop("disabled", false);
                            alert("error intente mas tarde");
                        });
                    });
                }
            }
        });
    });
}

function inpt(){
    var c3 = 2;
    jQuery(".inp[name=rad86]").off("change");
    jQuery(".inp[name=rad86]").on("change", function() {
        if(jQuery(this).val() == "Si"){
            if (jQuery(".inp[name=rad91]:checked").val() == "Si") {
                c3 = 10;
            } else if (jQuery(".inp[name=rad91]:checked").val() == "No"){
                c3 = 6;
            } else {
                c3 = 6;
            }
            for(var c = 87;c <= 90; c++){
                jQuery(".row" + c).css("display","block");
                jQuery(".inp[name=btn" + c + "]").prop("disabled", false);
            }
        } else if( jQuery(this).val() == "No") {
            if (jQuery(".inp[name=rad91]:checked").val() == "Si") {
                c3 = 6;
            } else  if (jQuery(".inp[name=rad91]:checked").val() == "No"){
                c3 = 2;
            } else {
                c3 = 2;
            }
            for(var c = 87;c <= 90; c++){
                jQuery(".row" + c).css("display","none");
                jQuery(".inp[name=btn" + c + "]").prop("disabled", true);
            }
        }
    });
    jQuery(".inp[name=rad91]").off("change");
    jQuery(".inp[name=rad91]").on("change", function() {
        if(jQuery(this).val() == "Si"){
            if (jQuery(".inp[name=rad86]:checked").val() == "Si") {
                c3 = 10;
            } else if (jQuery(".inp[name=rad86]:checked").val() == "No") {
                c3 = 6;
            } else {
                c3 = 6;
            }
            for(var c = 92;c <= 95; c++){
                jQuery(".row" + c).css("display","block");
                jQuery(".inp[name=btn" + c + "]").prop("disabled", false);
             }
        } else if( jQuery(this).val() == "No") {
            if (jQuery(".inp[name=rad86]:checked").val() == "Si") {
                c3 = 6;
            } else if (jQuery(".inp[name=rad86]:checked").val() == "No") {
                c3 = 2;
            } else {
                c3 = 2;
            }
            for(var c = 92;c <= 95; c++){
                jQuery(".row" + c).css("display","none");
                jQuery(".inp[name=btn" + c + "]").prop("disabled", true);
            }
        }
    });

    jQuery(".inp").on("change", function() {
        var c2 = 0;
        jQuery(".inp:checked:not(:disabled)").each( function(){
            c2++;
            if(c3 == c2) {
                jQuery(".dng").prop("disabled", false);
            } else {
                jQuery(".dng").prop("disabled", true);
            }
        });
    });
    jQuery(".dng").removeClass("btn-danger");
    jQuery(".dng").addClass("btn-success");
    jQuery(".dng").html("Guardar");
    jQuery(".dng").off("click").on("click", function() {
        jQuery(this).prop("disabled", true);
        jQuery(".inp:checked:not(:disabled)").each( function(){
            dta[jQuery(this).data('id')] = jQuery(this).val();
        });
        dta['user'] = jQuery("#us-id").val();
        jQuery.ajax({
            method: "POST",
            dataType: "json",
            data: dta,
            url: "save.php"
          }).done(function(data) {
            if(data){
                window.location.href = "finalizar.php";
            }else{
                jQuery(this).prop("disabled", false);
                alert("error intente mas tarde");
            }
          }).fail(function() {
            jQuery(this).prop("disabled", false);
            alert("error intente mas tarde");
        });
    });
}

function cargar() {
    car++;
    if(car <= 4) {
        jQuery(".title").find("h2").html("CUESTIONARIO PARA IDENTIFICAR A LOS TRABAJADORES QUE FUERON SUJETOS A ACONTECIMIENTOS TRAUMÁTICOS SEVEROS");
    } else {
        jQuery(".title").find("h2").html("CUESTIONARIO PARA IDENTIFICAR LOS FACTORES DE RIESGO PSICOSOCIAL Y EVALUAR EL ENTORNO ORGANIZACIONAL EN LOS CENTROS DE TRABAJO");
    }
    jQuery.ajax({
        method: "POST",
        dataType: "json",
        data: {
            section:car
        },
        url: "getseccion.php"
      }).done(function(data) {
          console.log(data);
        var frm = '';
        var c = 0;
        
        jQuery(".subtitle").find("h3").html(data['title']);
        for(d in data['form']){
            var tipop = '';
            var dis = "block";
            var disa = '';
            if(data['form'][d].type == 1){
                if (car == 17) {
                    dis = "block";
                }else if(car == 1 && data['form'][d].id != 2){
                    dis = "none";
                    disa = "disabled";
                }
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="rad' + data['form'][d].id + '" value="Si">Si</label>';
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="rad' + data['form'][d].id + '" value="No">No</label>';
            } else {
                if (car == 17 ) {
                    dis = "none";
                    disa = "disabled";
                    
                }
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="btn' + data['form'][d].id + '" value="Siempre">Siempre</label>';
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="btn' + data['form'][d].id + '" value="Casi Siempre">Casi Siempre</label>';
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="btn' + data['form'][d].id + '" value="Algunas Veces">Algunas Veces</label>';
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="btn' + data['form'][d].id + '" value="Casi Nunca">Casi Nunca</label>';
                tipop += '<label class="radio-inline"><input ' + disa + ' class="inp" type="radio" data-id="' + data['form'][d].id + '" name="btn' + data['form'][d].id + '" value="Nunca">Nunca</label>';
            }
            if(data['form'][d].type == 0) {
                frm += '<div class="row row-pregunta">';
                frm += '<div class="col-xs-12 col-md-12">' + data['form'][d].description + '</div>';
                frm += '</div>';
            } else {
                frm += '<div class="row row-pregunta row' + data['form'][d].id + '" style="display: ' + dis + '">';
                frm += '<div class="col-xs-12 col-md-6">' + data['form'][d].description + '</div>';
                frm += '<div class="col-xs-12 col-md-6">' + tipop + '</div>';
                frm += '</div>';
                c++;
            }
           
        }
        jQuery("#formulario").find(".row:first").html(frm);
        jQuery(".dng").prop("disabled", true);
        if (car == 17) {
            inpt();
        } else {
            inp(c);
        }
        
      });
}
