<?php
session_start();
include('conexion.php');
if(!isset($_SESSION['user'])){
    header("Location: login.php");
    exit;
}

?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body class="menu">
        <img src="assets/KSAPP_LOGOTIPO.png" id="logoizq" />
        <img src="assets/jp.jpg" id="idioma" />
        <br />
        <br />
        <div class="row">
                <h1><?php echo $_SESSION['user'] ?></h1>
            </div>
        <div class="container">
            <br />
            <h1>MENÚ</h1>
            <br />
            <br />

            <div class="row">
                <a href="evaluacion.php"><button class="btn btn-primary btn-menu">EVALUACIÓN</button></a>
            </div>
            <div class="row">
                <button class="btn btn-primary btn-menu">HISTORIAL DE <br />REGISTRO</button>
            </div>
            <div class="row">
                <a href="logout.php"><button class="btn btn-primary btn-menu">CERRAR SESIÓN</button></a>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 

    </script>
</htmL>