<?php
    session_start();
    
    if(isset($_SESSION['user'])){
        header("Location: index.php");
        exit;
    }
?>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <img src="assets/jp.jpg" id="idioma" />
        <div class="container">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="row">
                <center><img src="assets/KSAPP_LOGOTIPO.png" id="logo" /></center>
            </div>
            <br />
            <br />
                <div class="row">
                    
                        <div class="row">
                            <div class='col-xs-offset-2 col-xs-8 txt-a'>NÚMERO DE NÓMINA:</div>
                        </div>
                        <br />
                        <div class="row">

                            <div class='col-xs-offset-2 col-xs-8'>
                                <input type="text" class="form-control" id="password" />
                            </div>
                          
                        </div>
                        <br />
                        
                </div>
                <div class="row">
                <center><a href="#" class="lnk"><button class='btn btn-primary btn-lg' id='btn-login' >Entrar</button></a></center>
            </div>
            <br /><br /><br />
            
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script> 
    jQuery(document).ready(function(){
        jQuery("#btn-login").on("click", function(){
            let pass = jQuery("#password");
            console.log(pass);
            jQuery.ajax({
                method: "POST",
                url: "https://portal.ksmex.com.mx/kys_app/login.php?user="+pass.val(),
                dataType:"json",
                data:{
                    user:pass.val()
                }
            })
            .done(function( msg ) {
                if(msg){
                    location.href="index.php?user="+pass.val()
                }else{
                    alert("Error al loguearse");
                }
            });

        });
    }); 
    </script>
</htmL>